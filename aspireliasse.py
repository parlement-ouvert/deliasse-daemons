#! /usr/bin/env python3


# Deliasse-Daemons -- Retrieve continuously amendments changes and other news from French Parliament servers.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/deliasse-daemons
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Récupère en permanence sur Eliasse les actualités sur les amendements et met à jour les fichiers JSON correspondants"""


import argparse
import json
import logging
import os
import shutil
import sys
import tempfile
import threading
import time

import requests


app_name = os.path.splitext(os.path.basename(__file__))[0]
context_by_organe = {}
legislature = 15
log = logging.getLogger(app_name)
organes = None
target_dir = None


def add_task(context, **kwargs):
    tasks = context['tasks']
    for task in tasks:
        for key, value in kwargs.items():
            if task.get(key) != value:
                break
        else:
            # Same task already exists. Don't duplicate it.
            break
    else:
        tasks.append(kwargs)


def get_amendements(context, bibard=None, bibard_suffixe=None, full=False, numeros=None):
    organe = context['organe']
    log.info('get_amendements(bibard={}, bibard_suffixe={}, organe={}, full={})'.format(
        bibard, bibard_suffixe, organe, full))
    assert bibard
    assert bibard_suffixe is not None
    bibard_suffixed = bibard + bibard_suffixe
    assert numeros is not None
    assert organe in organes
    remaining_numeros = numeros
    json_dir = os.path.join(target_dir, 'assemblee', str(
        legislature), organe, bibard + bibard_suffixe)
    os.makedirs(json_dir, exist_ok=True)
    existing_filenames = set(
        filename
        for filename in os.listdir(json_dir)
        if filename.startswith('amendement-') and filename.endswith('.json')
    )
    while remaining_numeros:
        numeros_to_request = remaining_numeros[:25]
        remaining_numeros = remaining_numeros[25:]
        response = requests.get(
            'http://eliasse.assemblee-nationale.fr/eliasse/amendement.do',
            params=dict(
                bibard=bibard,
                bibardSuffixe=bibard_suffixe,
                legislature=legislature,
                numAmdt=numeros_to_request,
                organeAbrv=organe,
                # &page=1&start=0&limit=25
            ),
        )
        data = response.json()
        assert sorted(data.keys()) == ['amendements'], sorted(data.keys())
        amendements = data['amendements']
        for amendement in amendements:
            numero = amendement['numeroReference']
            json_filename = 'amendement-{}.json'.format(numero)
            write_json(amendement, os.path.join(json_dir, json_filename))
            existing_filenames.discard(json_filename)

            context['sort_by_numero_by_bibard_suffixed'][bibard_suffixed][numero] = amendement['sortEnSeance']

    if full:
        for json_filename in existing_filenames:
            json_file_path = os.path.join(json_dir, json_filename)
            log.info('  Removing obsolete amendment: {}'.format(json_file_path))
            os.remove(json_file_path)


def get_discussion(context, bibard=None, bibard_suffixe=None):
    organe = context['organe']
    log.info('get_discussion(bibard={}, bibard_suffixe={}, organe={})'.format(
        bibard, bibard_suffixe, organe))
    assert bibard
    assert bibard_suffixe is not None
    bibard_suffixed = bibard + bibard_suffixe
    response = requests.get(
        'http://eliasse.assemblee-nationale.fr/eliasse/discussion.do',
        params=dict(
            bibard=bibard,
            bibardSuffixe=bibard_suffixe,
            legislature=legislature,
            organeAbrv=organe,
        ),
    )
    data = response.json()
    assert sorted(data.keys()) == [
        'amdtsParOrdreDeDiscussion'], sorted(data.keys())
    discussion = data['amdtsParOrdreDeDiscussion']
    write_json(discussion, os.path.join(
        target_dir, 'assemblee', str(legislature), organe, bibard + bibard_suffixe, 'discussion.json'))

    context['numeros_by_bibard_suffixed'][bibard_suffixed] = numeros = [
        resume_amendement['numero']
        for resume_amendement in discussion['amendements']
    ]
    context['sort_by_numero_by_bibard_suffixed'][bibard_suffixed] = {
        resume_amendement['numero']: resume_amendement['sort']
        for resume_amendement in discussion['amendements']
    }
    add_task(
        context,
        function=get_amendements,
        bibard=bibard,
        bibard_suffixe=bibard_suffixe,
        full=True,
        numeros=numeros,
    )

    timer = threading.Timer(900, add_task, args=(context,), kwargs=dict(
        function=get_discussion,
        bibard=bibard,
        bibard_suffixe=bibard_suffixe,
    ))
    timer.start()


def get_prochain_a_discuter(context):
    organe = context['organe']
    log.info('get_prochain_a_discuter(organe={})'.format(organe))
    response = requests.get(
        'http://eliasse.assemblee-nationale.fr/eliasse/prochainADiscuter.do',
        cookies={'FOSUSED_ORGANE': organe}
    )
    data = response.json()
    # {
    #     "prochainADiscuter": {
    #         "bibard": "530",
    #         "bibardSuffixe": "",
    #         "legislature": "15",
    #         "nbrAmdtRestant": "0",
    #         "numAmdt": "CL28",
    #         "organeAbrv": "CION_LOIS",
    #     },
    # }
    assert sorted(data.keys()) == ['prochainADiscuter']
    data = data['prochainADiscuter']
    write_json(data, os.path.join(target_dir, 'assemblee', str(
        legislature), organe, 'prochain-a-discuter.json'))

    if int(data['legislature']) == legislature:
        bibard_suffixed = data['bibard'] + data['bibardSuffixe']
        numeros = context['numeros_by_bibard_suffixed'].get(bibard_suffixed)
        if numeros is not None:
            numero_a_discuter = data['numAmdt']
            if numero_a_discuter in numeros:
                index = numeros.index(numero_a_discuter)
                numeros_without_sort = []
                sort_by_numero = context['sort_by_numero_by_bibard_suffixed'][bibard_suffixed]
                for numero in numeros[:index]:
                    if not sort_by_numero[numero]:
                        numeros_without_sort.append(numero)
                if numeros_without_sort:
                    context['urgent_task'] = dict(
                        function=get_amendements,
                        bibard=data['bibard'],
                        bibard_suffixe=data['bibardSuffixe'],
                        full=False,
                        numeros=numeros_without_sort +
                        numeros[index:index + 2],
                    )

    timer = threading.Timer(2, set_need, (context, 'need_prochain_a_discuter'))
    timer.start()


def get_references_organes():
    log.info('get_references_organes()')
    response = requests.get(
        'http://eliasse.assemblee-nationale.fr/eliasse/getListeReferenceDesOrganes.do',
    )
    data = response.json()
    # [
    #     {
    #         "text":"Séance",
    #         "value":"AN"
    #     },{
    #         "text":"Toutes commissions",
    #         "value":"CION_TOUTE"
    #     },
    #     ...
    # ]

    write_json(data, os.path.join(
        target_dir, 'assemblee', str(legislature), 'organes.json'))

    global organes
    organes = [
        reference['value']
        for reference in data
    ]


def get_textes_ordre_du_jour(context):
    organe = context['organe']
    log.info('get_textes_ordre_du_jour(organe={})'.format(organe))
    response = requests.get(
        'http://eliasse.assemblee-nationale.fr/eliasse/textesOrdreDuJour.do',
        params=dict(
            organeAbrv=organe,
        ),
    )
    data = response.json()
    # [
    #     {
    #         "textBibard": "490",
    #         "textBibardSuffixe": "",
    #         "textTitre": "Protection des données personnelles"
    #     },
    #     {
    #         "textBibard": "539",
    #         "textBibardSuffixe": "",
    #         "textTitre": "Élection des représentants au Parlement européen"
    #     }
    # ]

    write_json(data, os.path.join(target_dir, 'assemblee',
                                  str(legislature), organe, 'ordre-du-jour.json'))

    # Remove obsolete discussions.
    bibard_suffixed_set = set(
        discussion['textBibard'] + discussion['textBibardSuffixe']
        for discussion in data
    )
    for bibard_suffixed in list(context['numeros_by_bibard_suffixed'].keys()):
        if bibard_suffixed not in bibard_suffixed_set:
            del context['numeros_by_bibard_suffixed'][bibard_suffixed]
    for bibard_suffixed in list(context['sort_by_numero_by_bibard_suffixed'].keys()):
        if bibard_suffixed not in bibard_suffixed_set:
            del context['sort_by_numero_by_bibard_suffixed'][bibard_suffixed]

    for discussion in data:
        add_task(
            context,
            function=get_discussion,
            bibard=discussion['textBibard'],
            bibard_suffixe=discussion['textBibardSuffixe'],
        )

    timer = threading.Timer(
        3600, set_need, (context, 'need_textes_ordre_du_jour'))
    timer.start()


def harvest_organe(organe):
    tasks = []
    context_by_organe[organe] = context = dict(
        need_prochain_a_discuter=True,
        need_textes_ordre_du_jour=True,
        numeros_by_bibard_suffixed={},
        organe=organe,
        sort_by_numero_by_bibard_suffixed={},
        tasks=tasks,
        urgent_task=None,
    )
    while True:
        if context['need_prochain_a_discuter']:
            context['need_prochain_a_discuter'] = False
            get_prochain_a_discuter(context)
            continue
        if context['need_textes_ordre_du_jour']:
            context['need_textes_ordre_du_jour'] = False
            get_textes_ordre_du_jour(context)
            continue
        task = context['urgent_task']
        if task is not None:
            context['urgent_task'] = None
            function = task.pop('function')
            function(context, **task)  # pylint: disable=E1134
            continue
        if tasks:
            task = tasks.pop(0)
            function = task.pop('function')
            function(context, **task)  # pylint: disable=E1134
            continue
        time.sleep(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true',
                        default=False, help='increase output verbosity')
    parser.add_argument(
        'target_dir',
        help='path of target directory containing JSON files',
    )
    args = parser.parse_args()

    global target_dir
    target_dir = args.target_dir

    logging.basicConfig(
        level=logging.INFO if args.verbose else logging.WARNING, stream=sys.stdout)

    get_references_organes()

    threads = [
        threading.Thread(target=harvest_organe, args=(organe,))
        for organe in organes
    ]
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()

    return 0


def set_need(context, need):
    context[need] = True


def write_json(data, file_path):
    dir = os.path.dirname(file_path)
    os.makedirs(dir, exist_ok=True)
    new_text = json.dumps(data, ensure_ascii=False, indent=2, sort_keys=True)
    if os.path.exists(file_path):
        with open(file_path, 'r', encoding='utf-8') as json_file:
            old_text = json_file.read()
    else:
        old_text = None
    if new_text != old_text:
        _, temp_file_path = tempfile.mkstemp(
            suffix='.json', prefix='eliasse-', text=True)
        with open(temp_file_path, 'w', encoding='utf-8') as json_file:
            json_file.write(new_text)
        shutil.move(temp_file_path, file_path)
        return True
    return False


if __name__ == '__main__':
    sys.exit(main())
