# Deliasse-Daemons

Retrieve continuously amendments changes and other news from French Parliament servers.

The data collected by Deliasse-Daemons are used by [Deliasse-API](https://framagit.org/parlement-ouvert/deliasse-api).

## Data & stream from _Senat_

cf [page of ameli database in Senat's open data web site](http://data.senat.fr/ameli/)

### Install `ameli` database from _Senat_

```bash
wget http://data.senat.fr/data/ameli/ameli.zip
unzip ameli.zip
su
su postgres
# dropdb ameli
# dropuser opendata
# createuser -IPRS opendata
#   Enter password for new role: opendata
#   Enter it again: opendata
# createdb -E utf-8 -O opendata ameli
psql ameli < var/opt/opendata/ameli.sql
exit
exit
rm -R var/
rm ameli.zip
```

### Schema of `ameli` database

![Schema of ameli SQL database](doc/ameli-schema.png)
