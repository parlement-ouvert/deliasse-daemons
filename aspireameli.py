#! /usr/bin/env python3


# Deliasse-Daemons -- Retrieve continuously amendments changes and other news from French Parliament servers.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/deliasse-daemons
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Récupère en permanence sur le site du Sénat les actualités sur les amendements et met à jour la base ameli."""


import argparse
import json
import logging
import os
import shutil
import sys
import tempfile
import threading
import time

import psycopg2
from psycopg2.extras import RealDictCursor
import requests


app_name = os.path.splitext(os.path.basename(__file__))[0]
connection = None
context_by_txt_id = {}
log = logging.getLogger(app_name)
nivrec_num_by_lib = None
sor_id_by_lib_and_typ = None


def add_task(context, **kwargs):
    tasks = context['tasks']
    for task in tasks:
        for key, value in kwargs.items():
            if task.get(key) != value:
                break
        else:
            # Same task already exists. Don't duplicate it.
            break
    else:
        tasks.append(kwargs)


def get_modifications_amendements(context):
    txt = context['txt']
    assert txt['txttyp'] in ('C', 'S')
    url = 'http://www.senat.fr/{}/{}/{}/modifs/JSON_{}.txt'.format(
        'encommission' if txt['txttyp'] == 'C' else 'enseance',
        txt['sesins']['lil'],
        txt['num'],
        txt['id'],
    )
    log.info('get_modifications_amendements({})'.format(url))
    response = requests.get(url)
    response.encoding = 'utf-8'  # Encoding returned by response is wrong.
    data = response.json()
    # print(json.dumps(data, ensure_ascii=False, indent=2, sort_keys=True))
    # {
    #   "Interventions_DG": [
    #     {
    #       "typeIntervention": "Intervention_DG",
    #       "amdid": "",
    #       "subid": "",
    #       "codeGroupe": "",
    #       "entlib": "Nicole&nbsp;BELLOUBET",
    #       "nom": "BELLOUBET",
    #       "prenom": "Nicole",
    #       "qualite": "Mme",
    #       "urlAuteur": "",
    #       "role": "garde des sceaux, ministre de la justice",
    #       "ordre": 1,
    #       "ordreDG": 0,
    #       "temps": 0,
    #       "lastOrateurAvecTemps": "N",
    #       "tempsTotal": "53'"
    #     },
    #     {
    #       "typeIntervention": "Intervention_DG",
    #       "amdid": "",
    #       "subid": "",
    #       "codeGroupe": "UC",
    #       "entlib": "Sophie&nbsp;JOISSAINS",
    #       "nom": "JOISSAINS",
    #       "prenom": "Sophie",
    #       "qualite": "Mme",
    #       "urlAuteur": "joissains_sophie08044w.html",
    #       "role": "rapporteur de la commission des lois",
    #       "ordre": 2,
    #       "ordreDG": 0,
    #       "temps": 10,
    #       "lastOrateurAvecTemps": "N",
    #       "tempsTotal": "53'"
    #     }
    #   ],
    #   "Interventions_articles": [],
    #   "Interventions_motions": [],
    #   "amendements": [],
    #   "subdivisions": [],
    #   "texte": {
    #     "idtxt": "103221",
    #     "libsortxt": "Le texte est adopté par la commission",
    #     "msgrefresh": "L'ordre de discussion des amendements a été modifié",
    #     "tpsmaj": 1521219047000,
    #     "tpsrefresh": 1521218813000
    #   }
    # }
    del data['texte']['tpsmaj']
    del data['texte']['tpsrefresh']
    if data != context['modifications_amendements']:
        context['modifications_amendements'] = data
        context['urgent_task'] = dict(
            function=get_liste_discussion,
        )

    timer = threading.Timer(2, set_need, (context, 'need_modifications_amendements'))
    timer.start()


def get_liste_discussion(context):
    txt = context['txt']
    assert txt['txttyp'] in ('C', 'S')
    url = 'http://www.senat.fr/{}/{}/{}/liste_discussion.json'.format(
        'encommission' if txt['txttyp'] == 'C' else 'enseance',
        txt['sesins']['lil'],
        txt['num'],
    )
    log.info('get_liste_discussion({})'.format(url))
    response = requests.get(url)
    data = response.json()
    # print(json.dumps(data, ensure_ascii=False, indent=2, sort_keys=True))
    # {
    #   "Subdivisions": [
    #     {
    #       "Amendements": [],
    #       "id_subdivision": "153960",
    #       "libelle_subdivision": "TITRE IER",
    #       "signet": "../../textes/2017-2018/296.html#AMELI_SUB_2__Titre_I"
    #     },
    #     {
    #       "Amendements": [],
    #       "id_subdivision": "153961",
    #       "libelle_subdivision": "Chapitre Ier",
    #       "signet": "../../textes/2017-2018/296.html#AMELI_SUB_3__Chapitre_I__Titre_I"
    #     },
    #     {
    #       "Amendements": [
    #         {
    #           "auteur": "Mme JOISSAINS, rapporteur",
    #           "idAmendement": "1110289",
    #           "idAmendementPere": "0",
    #           "idDiscussionCommune": "110563",
    #           "isAdopte": "true",
    #           "isDiscussionCommune": "true",
    #           "isDiscussionCommuneIsolee": "false",
    #           "isIdentique": "false",
    #           "isRejete": "false",
    #           "isSousAmendement": "false",
    #           "libelleAlinea": "Al. 4",
    #           "num": "COM-28",
    #           "posder": "1",
    #           "sort": "Adopté",
    #           "subpos": "0",
    #           "typeAmdt": "Amt",
    #           "urlAmdt": "Amdt_COM-28.html",
    #           "urlAuteur": "joissains_sophie08044w.html"
    #         },
    #         {
    #           "auteur": "M. PATRIAT",
    #           "idAmendement": "1110239",
    #           "idAmendementPere": "0",
    #           "idDiscussionCommune": "110563",
    #           "isAdopte": "false",
    #           "isDiscussionCommune": "true",
    #           "isDiscussionCommuneIsolee": "false",
    #           "isIdentique": "false",
    #           "isRejete": "false",
    #           "isSousAmendement": "false",
    #           "libelleAlinea": "Al. 4",
    #           "num": "COM-27",
    #           "posder": "2",
    #           "sort": "Satisfait ou sans objet",
    #           "subpos": "0",
    #           "typeAmdt": "Amt",
    #           "urlAmdt": "Amdt_COM-27.html",
    #           "urlAuteur": "patriat_francois08061x.html"
    #         }
    #       ],
    #       "id_subdivision": "153962",
    #       "libelle_subdivision": "Article 1er",
    #       "signet": "../../textes/2017-2018/296.html#AMELI_SUB_4__Article_1"
    #     }
    #   ],
    #   "info_generales": {
    #     "doslegsignet": "pjl17-296",
    #     "idtxt": "103221",
    #     "intituleLoi": "Protection des données personnelles",
    #     "lecture": "1ère lecture",
    #     "natureLoi": "Projet de loi",
    #     "nbAmdtsAExaminer": "0",
    #     "nbAmdtsDeposes": "91",
    #     "tsgenhtml": "1521219047000"
    #   }
    # }
    cursor = context['cursor']
    for subdivision in data['Subdivisions']:
        for amendement in subdivision['Amendements']:
            amd = {}
            for key, value in amendement.items():
                if key == 'auteur':
                    pass
                elif key == 'idAmendement':
                    amd['id'] = int(value)
                elif key == 'idAmendementPere':
                    amd['amdperid'] = int(value) or None
                elif key == 'idDiscussionCommune':
                    amd['discomid'] = int(value)
                elif key == 'idIdentique':
                    amd['ideid'] = int(value)
                elif key == 'isAdopte':
                    assert value in ('false', 'true'), amendement
                elif key == 'isDiscussionCommune':
                    assert value in ('false', 'true'), amendement
                    if value == 'false':
                        amd['discomid'] = None
                elif key == 'isDiscussionCommuneIsolee':
                    # TODO
                    assert value in ('false', 'true'), amendement
                    # if value == 'false':
                    #     amd['discomid'] = None
                elif key == 'isIdentique':
                    assert value in ('false', 'true'), amendement
                    if value == 'false':
                        amd['ideid'] = None
                elif key == 'isRejete':
                    assert value in ('false', 'true'), amendement
                elif key == 'isSousAmendement':
                    assert value in ('false', 'true'), amendement
                    if value == 'false':
                        amd['amdperid'] = None
                elif key == 'libelleAlinea':
                    if value:
                        if value in ('Réd. glob.', 'Suppr.'):
                            # TODO
                            pass
                        else:
                            assert value.startswith('Al. '), amendement
                            amd['alinea'] = int(value[4:])
                    else:
                        amd['alinea'] = None
                elif key == 'num':
                    if ' ' in value:
                        num, remaining = value.split(None, 1)
                        assert remaining.startswith('rect.'), amendement
                        if ' ' in remaining:
                            nivrec_lib = remaining.split(None, 1)[1]
                            rectification = nivrec_num_by_lib[nivrec_lib]
                        else:
                            rectification = 1
                        amd['num'] = num
                        amd['rev'] = rectification
                    else:
                        amd['num'] = value
                        amd['rev'] = 0
                elif key == 'posder':
                    # assert int(value) == index, amendement
                    # TODO
                    pass
                elif key == 'sort':
                    amd['sorid'] = sor_id_by_lib_and_typ[(value, txt['txttyp'])]
                elif key == 'subpos':
                    amd['subpos'] = int(value)
                elif key == 'typeAmdt':
                    assert value in ('Amt', 'S/Amt'), amendement
                    # TODO
                elif key == 'urlAmdt':
                    pass
                elif key == 'urlAuteur':
                    # TODO
                    pass
                else:
                    raise KeyError('Unexpected key "{}" in {}'.format(key, amendement))
            cursor.execute('SELECT * FROM amd WHERE id = %(id)s', amd)
            existing_amd = cursor.fetchone()
            if existing_amd is None:
                # For example, a sous-amendement added the same day
                amd['etaid'] = 7  # TODO: This is a "random" number.
                amd['nomentid'] = 0  # TODO: This is a "random" number.
                amd['txtid'] = txt['id']
                command = 'INSERT INTO amd ({}) VALUES ({})'.format(
                    ', '.join(amd.keys()),
                    ', '.join(
                        '%({key})s'.format(key=key)
                        for key in amd.keys()
                    ),
                )
                log.info('Nouvel amendement inséré : {}'.format(amendement))
                log.info(cursor.mogrify(command, amd))
                cursor.execute(command, amd)
            else:
                changes = {}
                for key, value in amd.items():
                    if value != existing_amd[key]:
                        changes[key] = value
                if changes:
                    command = 'UPDATE amd SET {} WHERE id = %(id)s'.format(', '.join(
                        '{key} = %({key})s'.format(key=key)
                        for key in changes.keys()
                    ))
                    changes['id'] = amd['id']
                    print(cursor.mogrify(command, changes))
                    cursor.execute(command, changes)



def harvest_modifications_amendements(txt):
    with connection.cursor() as cursor:
        context_by_txt_id[txt['id']] = context = dict(
            cursor=cursor,
            modifications_amendements=None,
            need_modifications_amendements=True,
            txt=txt,
            urgent_task=None,
        )
        while True:
            if context['need_modifications_amendements']:
                context['need_modifications_amendements'] = False
                get_modifications_amendements(context)
                continue
            task = context['urgent_task']
            if task is not None:
                context['urgent_task'] = None
                function = task.pop('function')
                function(context, **task)  # pylint: disable=E1134
                continue
            time.sleep(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true',
                        default=False, help='increase output verbosity')
    parser.add_argument(
        'ameli_db_url',
        help='URL of PostgreSQL ameli database',
    )
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.INFO if args.verbose else logging.WARNING, stream=sys.stdout)

    global connection
    connection = psycopg2.connect(args.ameli_db_url, cursor_factory=RealDictCursor)
    cursor = connection.cursor()


    cursor.execute('SELECT * FROM w_nivrec')
    global nivrec_num_by_lib
    nivrec_num_by_lib = {
        entry['lib']: entry['num']
        for entry in cursor.fetchall()
    }


    cursor.execute('SELECT * FROM sor')
    global sor_id_by_lib_and_typ
    sor_id_by_lib_and_typ = {
        (entry['lib'], entry['typ']): entry['id']
        for entry in cursor.fetchall()
    }

    txts = []
    for txt_id in (103221, 103234):
        cursor.execute('SELECT * FROM txt_ameli WHERE id = %s', (txt_id, ))
        txt = cursor.fetchone()
        cursor.execute('SELECT * FROM ses WHERE id = %s', (txt['sesinsid'], ))
        txt['sesins'] = cursor.fetchone()
        txts.append(txt)

    threads = [
        threading.Thread(target=harvest_modifications_amendements, args=(txt,))
        for txt in txts
    ]
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()

    cursor.close()
    connection.close()

    return 0


def set_need(context, need):
    context[need] = True


if __name__ == '__main__':
    sys.exit(main())
